const serviceBtns = document.querySelectorAll(".service-btn");
const serviceArticles = document.querySelectorAll(".image-article-item");

const amazingBtns = document.querySelectorAll(".amazing-btn");
const amazingPictures = document.querySelectorAll(".amazing-work-wrapper");
const amazingContainer = document.querySelector(".amazing-work-pictures");
const loadBtn = document.querySelector(".load-button");

const bigIcons = document.querySelectorAll("#big-icons");

serviceBtns.forEach((button) => {
  button.addEventListener("click", (e) => {
    serviceBtns.forEach((btn) => {
      if (e.target === btn) {
        btn.classList.add("service-active");
      } else {
        btn.classList.remove("service-active");
      }
    });

    serviceArticles.forEach((contentItem) => {
      if (e.target.dataset.attr === contentItem.dataset.attr) {
        contentItem.classList.replace("hidden", "visible");
      } else {
        contentItem.classList.replace("visible", "hidden");
      }
    });
  });
});

loadBtn.onclick = function () {
  amazingPictures.forEach((contentItem) => {
    contentItem.classList.remove("hidden");
  });
};

amazingBtns.forEach((button) => {
  button.addEventListener("click", (e) => {
    amazingBtns.forEach((btn) => {
      if (e.target === btn) {
        btn.style.cssText = `color: var(--primary-color);
        box-shadow: 0 0 0 2px inset var(--primary-color);`;
      } else {
        btn.style.cssText = ``;
      }
    });

    amazingPictures.forEach((contentItem) => {
      if (e.target.dataset.filter !== contentItem.dataset.filter) {
        contentItem.classList.add("hidden");
      } else {
        contentItem.classList.remove("hidden");
      }
      if (e.target.dataset.all === contentItem.dataset.all) {
        contentItem.classList.remove("hidden");
      }
    });
  });
});

$(document).ready(function () {
  $(".right").on("click", function () {
    let current = $(".active");
    let current1 = $(".active1");
    let next = current.next();
    let next1 = current1.next();

    if (next.length) {
      current.removeClass("active");
      current1.removeClass("active1");
      current.addClass("hidden");
      next1.addClass("active1");
      next.addClass("active");
    }
  });
  $(".left").on("click", function () {
    let current = $(".active");
    let current1 = $(".active1");
    let prev = current.prev();
    let prev1 = current1.prev();

    if (prev.length) {
      current.removeClass("active");
      current1.removeClass("active1");
      prev1.addClass("active1");
      prev.addClass("active");
    }
  });
});
